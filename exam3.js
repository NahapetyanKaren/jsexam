let m = [5, 8, 4, 1, 0, 45, 12, 99, 24, -10];
function arrSorted(arr) {
    for (i = 0; i < arr.length; i++) {
        for (j = 0; j < arr.length; j++) {
            if (arr[i] < arr[j]) {
                let x = arr[i];
                arr[i] = arr[j];
                arr[j] = x;
            }
        }
    }
    return arr;
}

console.log(arrSorted(m));