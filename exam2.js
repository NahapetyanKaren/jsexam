
let a = [1, 3, 6, 7, 11, 13, 17, 20]
let b = [2, 4, 5, 8, 9, 10, 18]

function sortArrAll(arr1, arr2) {
    let arr3 = [];
    let x = 0 // for arr1
    let y = 0 // for arr2

    while (x < arr1.length && y < arr2.length) {
        if (arr1[x] < arr2[y]) {
            arr3.push(arr1[x]);
            x++
        } else {
            arr3.push(arr2[y]);
            y++
        }       
    }

    while (x < arr1.length) {
        arr3.push(arr1[x])
        x++
    }

    while (y < arr2.length) {
        arr3.push(arr2[y])
        y++
    }

return arr3;

}

console.log(sortArrAll(a, b))